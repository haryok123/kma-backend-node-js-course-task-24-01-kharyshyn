const express = require('express')
const app = express()
const port = parseInt(process.env.PORT, 10) || 56201;
const bodyParser = require('body-parser');
app.use(bodyParser.text());
app.post('/square', (req, res) => {

    const number = +req.body;

    if (isNaN(number)) {
        return res.status(400).json({ error: 'Invalid input. You have to use numbers' });
    }

    return res.json({
        number: number,
        square:  number**2
    })
})

app.post('/reverse', (req, res) => {
    
    let reversedString = [...req.body].reverse().join('');
    return res.send(reversedString);
})

app.get('/date/:year/:month/:day', (req, res) => {
    const { year, month, day } = req.params;

    const date = new Date(`${year}-${month}-${day}`);

    if (isNaN(date.getTime())) {
        return res.status(400).json({ error: 'Invalid date format' });
    }

    const isLeapYear = ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);
    let daysInFebrurary = isLeapYear ? 29: 28;

    const arrOfDaysInMonth = [31, daysInFebrurary, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    if (Number(day) > arrOfDaysInMonth[Number(month)-1]) {
        return res.status(400).json({ error: `Invalid date format. There are ${arrOfDaysInMonth[Number(month)-1]} in ${month} month` });
    }

    const arrOfDays = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const weekDay = arrOfDays[date.getDay()];

    

    const currentDate = new Date();

    const difference = Math.abs(Math.ceil((date - currentDate) / (1000 * 60 * 60 * 24)));

    res.json({ 
        weekDay: weekDay, 
        isLeapYear: isLeapYear, 
        difference: difference
    });
});

app.listen(port, () => {
    console.log(`app listening on port ${port}`);
})